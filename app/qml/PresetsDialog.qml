import QtQuick 1.1
import com.nokia.meego 1.0

SelectionDialog
{
    id: presetsDialog

    titleText: "Well-known user agents"
    onAccepted:
    {
        var a = model.get(selectedIndex).name
        var b = model.get(selectedIndex).userAgent
        manuallyDetailsSheet.importData(a, b)
    }

    model: ListModel
    {
        ListElement
        {
            name: "Nokia N900"
            userAgent: "Mozilla/5.0 (X11; U; Linux armv7l; en-US; rv:1.9.3pre) Gecko/20100723 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900"
        }
        ListElement
        {
            name: "Nokia N9"
            userAgent: "Mozilla/5.0 (MeeGo; NokiaN9) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13"
        }
        ListElement
        {
            name: "Apple iPhone 4S"
            userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"
        }
        ListElement
        {
            name: "HP Pre 3"
            userAgent: "Mozilla/5.0 (Linux; webOS/2.2.4; U; en-US) AppleWebKit/534.6 (KHTML, like Gecko) webOSBrowser/221.56 Safari/534.6 Pre/3.0"
        }
        ListElement
        {
            name: "Samsung Galaxy S3"
            userAgent: "Mozilla/5.0 (Linux; U; Android 4.0.4; en-US; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
        }
        ListElement
        {
            name: "HTC Desire Z"
            userAgent: "Mozilla/5.0 (Linux; U; Android 2.2; en-US; HTC Desire Z 1.34.161.6 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
        }
        ListElement
        {
            name: "Nokia Lumia 800"
            userAgent: "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; NOKIA; Lumia 800)"
        }
        ListElement
        {
            name: "Nokia 701 (Symbian Belle)"
            userAgent: "Mozilla/5.0 (Symbian/3; Series60/5.3 Nokia701/111.020.0307; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.4.1.14 Mobile Safari/533.4 3gpp-gba"
        }
        ListElement
        {
            name: "BlackBerry 9900"
            userAgent: "Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+"
        }
        ListElement
        {
            name: "Sony Ericsson U10i Aino"
            userAgent: "SonyEricssonU10i/R1A Browser/NetFront/3.5 Profile/MIDP-2.1 Configuration/CLDC-1.1"
        }
        ListElement
        {
            name: "Sony Ericsson K800i"
            userAgent: "SonyEricssonK800i/R1CF Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0"
        }
        ListElement
        {
            name: "Maemo Fennec 6"
            userAgent: "Mozilla/5.0 (Maemo; Linux armv7l; rv:6.0a1) Gecko/20110526 Firefox/6.0a1 Fennec/6.0a1"
        }
        ListElement
        {
            name: "Opera Mobile 12"
            userAgent: "Opera/9.80 (Android 4.1.1; Linux; Opera Mobi/ADR-1205181138; U; en-GB) Presto/2.10.254 Version/12.00"
        }
        ListElement
        {
            name: "Opera Mini 9.80"
            userAgent: "Opera/9.80 (J2ME/MIDP; Opera Mini/9.80 (S60; SymbOS; Opera Mobi/23.348; U; en) Presto/2.5.25 Version/10.54"
        }
        ListElement
        {
            name: "Google Chrome"
            userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5"
        }
    }

}
