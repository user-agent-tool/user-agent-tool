#include "UserAgentChanger.h"
#include <QFile>
#include <QTextStream>

QString UserAgentChanger::getCurrentAgent()
{
    QFile userjs("/home/user/.mozilla/microb/user.js");
    userjs.open(QFile::ReadOnly);

    QTextStream str(&userjs);
    QString uadata = str.readAll();

    userjs.close();
    if (userjs.error() != QFile::NoError)
    {
        emit error(userjs.errorString());
        return "";
    }
    return uadata.section('\"', 3, 3);
}

void UserAgentChanger::setCurrentAgent(QString agent)
{
    QFile userjs("/home/user/.mozilla/microb/user.js");
    userjs.open(QFile::WriteOnly | QFile::Truncate);

    QTextStream str(&userjs);
    QString uadata = QString("user_pref(\"general.useragent.override\", \"%1\");").arg(agent);
    str << uadata;

    userjs.close();

    if (userjs.error() != QFile::NoError)
    {
        emit error(userjs.errorString());
        return;
    }
    restartBrowserUI();
    emit currentAgentChanged(agent);
    emit writtenOK();
}
