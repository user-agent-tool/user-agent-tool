TEMPLATE = lib
SOURCES = plugin.c
TARGET = UserAgentTool

CONFIG += plugin link_pkgconfig
PKGCONFIG += gtk+-2.0 libosso

# dummy 'install'
# target.path = $$[QT_INSTALL_EXAMPLES]/maemo5/controlpanel/qtpanelplugin
# sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS qtpanelplugin.pro qtpanelplugin.desktop
# sources.path = $$[QT_INSTALL_EXAMPLES]/maemo5/controlpanel/qtpanelplugin
# INSTALLS += target sources

# for a real install, you would want to use something like this:
 # install
desktopfile.path = /usr/share/applications/hildon-control-panel/
desktopfile.files = $${TARGET}.desktop
target.path = /opt/UserAgentTool/lib/
INSTALLS += target desktopfile
