#ifndef USERAGENTCHANGER_H
#define USERAGENTCHANGER_H

#include <QObject>
#include <cstdlib>
#include <string>

using namespace std;

class UserAgentChanger : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString currentAgent READ getCurrentAgent WRITE setCurrentAgent NOTIFY currentAgentChanged)
public:
    void setCurrentAgent(QString agent);
    QString getCurrentAgent();
    Q_INVOKABLE inline bool restartBrowserUI() { return system("killall browserui"); }
    Q_INVOKABLE inline int startBrowser(QString url)
    {
        string cmd("run-standalone.sh dbus-send --system --type=method_call --dest=com.nokia.osso_browser /com/nokia/osso_browser/request com.nokia.osso_browser.load_url string:\"" + url.toStdString() + "\"");
        return system(cmd.c_str());
    }

protected:
    QString agent;
signals:
    void agentChanged(QString a);
    void currentAgentChanged(QString a);
    void error(QString desc);
    void writtenOK();
};

#endif // USERAGENTCHANGER_H
