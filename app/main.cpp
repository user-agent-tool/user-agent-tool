#include <QtGui/QApplication>
#include <QtDeclarative>
#include "qmlapplicationviewer.h"
#include "UserAgentChanger.h"


int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    qmlRegisterType<UserAgentChanger>("MicroBAgentSwitcher", 1, 0, "UserAgentChanger");

    QmlApplicationViewer viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockLandscape);
    viewer.setMainQmlFile(QLatin1String("qml/main.qml"));
    viewer.showFullScreen();

    return app.exec();
}
