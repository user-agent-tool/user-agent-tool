import QtQuick 1.1
import com.nokia.meego 1.0


Sheet
{
    id: browserURLSheet

    function reset()
    {
        urlField.text = ""
    }

    rejectButtonText: "Cancel"
    acceptButtonText: "OK"
    onAccepted:
    {
        uachanger.startBrowser(urlField.text)
        reset()
    }
    onRejected: reset()

    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        TextField
        {
            id: urlField
            placeholderText: "Web address"
            width: parent.width
        }
    }
}
