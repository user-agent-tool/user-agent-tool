import QtQuick 1.1

Item
{
    width: parent.width
    height: 75
    property alias headertext: headertxt.text
    Rectangle
    {
        id: maelyricaheader
        color: "#4591ff"
        anchors.top: parent.top
        height: 60
        width: parent.width
        Text
        {
            width: parent.width
            id: headertxt
            color: "white"
            anchors
            {
                verticalCenter: parent.verticalCenter
                left: parent.left
                leftMargin: 30
            }
            font.pixelSize: 36
        }
    }
}
