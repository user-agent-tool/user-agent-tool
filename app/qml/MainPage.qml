import QtQuick 1.1
import com.nokia.meego 1.0
import com.nokia.extras 1.0


Page
{
    id: mainPage
    tools: commonTools

    Component.onCompleted: getList()

    function getDatabase() {return openDatabaseSync("UserAgentTool", 1.0, "User Agent Tool", 10000); }

    function getList()
    {
        var db = getDatabase();
        var ret
        db.transaction( function (tx)
        {
            ret = tx.executeSql("SELECT * FROM UserAgents")
        })

        uamodel.clear()
        for (var i = 0; i < ret.rows.length; ++i)
        {
            uamodel.append({"ualabel" : ret.rows.item(i).ualabel, "uastring" : ret.rows.item(i).uastring})
        }
    }



    function addToList(ualabel, uastring)
    {
        var db = getDatabase();
        db.transaction(function (tx)
        {
            tx.executeSql('CREATE TABLE IF NOT EXISTS UserAgents(ualabel TINYTEXT UNIQUE, uastring TEXT)')
            var ret = tx.executeSql("INSERT OR REPLACE INTO UserAgents VALUES(?, ?)", [ualabel, uastring])
            if (ret.rowsAffected <= 0) sqlError();
        })

        listChanged()
        
    }

    function deleteFromList(ualabel) {}

    signal sqlError()
    signal listChanged()

    onSqlError:
    {
        errorBanner.details = " accessing the SQL database"
        errorBanner.show()
    }

    onListChanged: getList()


    ListModel
    {
        id: uamodel
    }

//    Flickable
//    {
//        id: flickDetails
//        anchors.fill: parent
//        contentHeight: deviceButtonColumn.height + 10 // some kinda margin
//        flickableDirection: Flickable.VerticalFlick
//        clip: true
//    }

    ListView
    {
        anchors.fill: parent
        id: uaview
        model: uamodel
        spacing: 10
        header: Header { headertext: "Select a user agent" }

        property int imgButtonWidth: parent.width / 8
        property int devButtonWidth: parent.width * 0.6

        delegate: Row
        {
            anchors.leftMargin: 10
            anchors.rightMargin: 10
            //anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width
            spacing: 15

            Item
            {
                //spacer
                height: 1
                width: (parent.width - 4 * parent.spacing - 2 * uaview.imgButtonWidth - uaview.devButtonWidth) / 2
            }

            Button
            {
                //Image {anchors.centerIn: parent; s
                iconSource: "image://theme/icon-m-toolbar-edit"
                width: uaview.imgButtonWidth
            }
            DeviceButton
            {
                device: model.ualabel;
                userAgent: model.uastring;
                width: uaview.devButtonWidth
            }
            Button
            {
                //Image { anchors.centerIn: parent;
                iconSource: "image://theme/icon-m-toolbar-delete"
                width: uaview.imgButtonWidth
            }
        }
    }

    Loader
    {
        id: myLoader
    }

    QueryDialog
    {
        id: uadialog
        titleText: "Your current user agent"
        message: uachanger.currentAgent
        rejectButtonText: "OK"
    }

    Connections
    {
        target: uachanger
        onError:
        {
            errorBanner.details = desc
            errorBanner.show()
        }
        onWrittenOK: writtenOKBanner.show()
    }

    InfoBanner
    {
        property string details
        id: errorBanner
        text: "Error: " + details
    }

    InfoBanner
    {
        id: writtenOKBanner
        text: "User agent changed successfully"
    }


    ToolBarLayout
    {
        id: commonTools
        visible: true

        ToolIcon
        {
            iconSource: "image://theme/icon-s-description"
            onClicked: uadialog.open()
        }

        ToolIcon
        {
            platformIconId: "toolbar-add"
            onClicked:
            {
                myLoader.source = "CustomAgentSheet.qml"
                myLoader.item.open()
            }
        }

        ToolIcon
        {
            platformIconId: "toolbar-view-menu";
            onClicked: (myMenu.status === DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }
    }

    Menu
    {
        id: myMenu
        visualParent: pageStack
        MenuLayout
        {
            MenuItem
            {
                text: "Restart BrowserUI"
                onClicked: uachanger.restartBrowserUI
            }

            MenuItem
            {
                text: "Launch browser"
                onClicked:
                {
                    myLoader.source = "BrowserURLSheet.qml"
                    myLoader.item.open()
                }
            }

            MenuItem
            {
                text: "About"
                onClicked:
                {
                    myLoader.source = "AboutDialog.qml"
                    myLoader.item.open()
                }
            }

            MenuItem
            {
                text: "Log current sqldb"
                onClicked: console.log(getList())
            }
        }
    }

}

//        Column
//        {
//            id: deviceButtonColumn
//            spacing: 10
//            width: parent.width
//            Header
//            {
//                headertext: "Select device's user agent"
//            }

//            DeviceButton
//            {
//                device: "Nokia N900"
//                userAgent: "Mozilla/5.0 (X11; U; Linux armv7l; en-US; rv:1.9.3pre) Gecko/20100723 Firefox/3.5 Maemo Browser 1.7.4.8 RX-51 N900"
//            }
//            DeviceButton
//            {
//                device: "Nokia N9"
//                userAgent: "Mozilla/5.0 (MeeGo; NokiaN9) AppleWebKit/534.13 (KHTML, like Gecko) NokiaBrowser/8.5.0 Mobile Safari/534.13"
//            }
//            DeviceButton
//            {
//                device: "Apple iPhone 4S"
//                userAgent: "Mozilla/5.0 (iPhone; CPU iPhone OS 5_0 like Mac OS X) AppleWebKit/534.46 (KHTML, like Gecko) Version/5.1 Mobile/9A334 Safari/7534.48.3"
//            }
//            DeviceButton
//            {
//                device: "HP Pre 3"
//                userAgent: "Mozilla/5.0 (Linux; webOS/2.2.4; U; en-US) AppleWebKit/534.6 (KHTML, like Gecko) webOSBrowser/221.56 Safari/534.6 Pre/3.0"
//            }
//            DeviceButton
//            {
//                device: "Samsung Galaxy S3"
//                userAgent: "Mozilla/5.0 (Linux; U; Android 4.0.4; en-US; GT-I9300 Build/IMM76D) AppleWebKit/534.30 (KHTML, like Gecko) Version/4.0 Mobile Safari/534.30"
//            }
//            DeviceButton
//            {
//                device: "HTC Desire Z"
//                userAgent: "Mozilla/5.0 (Linux; U; Android 2.2; en-US; HTC Desire Z 1.34.161.6 Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1"
//            }

//            DeviceButton
//            {
//                device: "Nokia Lumia 800"
//                userAgent: "Mozilla/5.0 (compatible; MSIE 9.0; Windows Phone OS 7.5; Trident/5.0; IEMobile/9.0; NOKIA; Lumia 800)"
//            }
//            DeviceButton
//            {
//                device: "Nokia 701 (Symbian Belle)"
//                userAgent: "Mozilla/5.0 (Symbian/3; Series60/5.3 Nokia701/111.020.0307; Profile/MIDP-2.1 Configuration/CLDC-1.1 ) AppleWebKit/533.4 (KHTML, like Gecko) NokiaBrowser/7.4.1.14 Mobile Safari/533.4 3gpp-gba"
//            }
//            DeviceButton
//            {
//                device: "BlackBerry 9900"
//                userAgent: "Mozilla/5.0 (BlackBerry; U; BlackBerry 9900; en) AppleWebKit/534.11+ (KHTML, like Gecko) Version/7.1.0.346 Mobile Safari/534.11+"
//            }
//            DeviceButton
//            {
//                device: "Sony Ericsson U10i Aino"
//                userAgent: "SonyEricssonU10i/R1A Browser/NetFront/3.5 Profile/MIDP-2.1 Configuration/CLDC-1.1"
//            }
//            DeviceButton
//            {
//                device: "Sony Ericsson K800i"
//                userAgent: "SonyEricssonK800i/R1CF Profile/MIDP-2.0 Configuration/CLDC-1.1 UNTRUSTED/1.0"
//            }

//            DeviceButton
//            {
//                device: "Maemo Fennec 6"
//                userAgent: "Mozilla/5.0 (Maemo; Linux armv7l; rv:6.0a1) Gecko/20110526 Firefox/6.0a1 Fennec/6.0a1"
//            }
//            DeviceButton
//            {
//                device: "Opera Mobile 12"
//                userAgent: "Opera/9.80 (Android 4.1.1; Linux; Opera Mobi/ADR-1205181138; U; en-GB) Presto/2.10.254 Version/12.00"
//            }

//            DeviceButton
//            {
//                device: "Opera Mini 9.80"
//                userAgent: "Opera/9.80 (J2ME/MIDP; Opera Mini/9.80 (S60; SymbOS; Opera Mobi/23.348; U; en) Presto/2.5.25 Version/10.54"
//            }
//            DeviceButton
//            {
//                device: "Google Chrome"
//                userAgent: "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/536.5 (KHTML, like Gecko) Chrome/19.0.1084.9 Safari/536.5"
//            }
//        }
