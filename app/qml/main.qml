import QtQuick 1.1
import com.nokia.meego 1.0
import MicroBAgentSwitcher 1.0

PageStackWindow
{
    id: appWindow

    initialPage: MainPage {}

    UserAgentChanger { id: uachanger }
}
