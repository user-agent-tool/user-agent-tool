import QtQuick 1.1
import com.nokia.meego 1.0


Sheet
{
    id: manuallyDetailsSheet

    function reset()
    {
        importData("", "")
    }

    function importData(a, b)
    {
        userAgentNameField.text = a
        userAgentStringField.text = b
    }

    Loader
    {
        id: presetsLoader
    }

    rejectButtonText: "Cancel"
    acceptButtonText: "OK"
    onAccepted:
    {
        // opacityAnimation.start()
        addToList(userAgentNameField.text, userAgentStringField.text)
        reset()
    }
    onRejected:
    {
        // opacityAnimation.start();
        reset();
    }

    content: Rectangle
    {
        anchors.fill: parent
        width: parent.width
        height: parent.height
        color: "#E0E1E2"
        Column
        {
            width: parent.width
            spacing: 30
            anchors.margins: 20
            TextField
            {
                id: userAgentNameField
                placeholderText: "Display name"
                width: parent.width
            }

            TextField
            {
                id: userAgentStringField
                placeholderText: "User agent string"
                width: parent.width
            }
        }
        Rectangle
        {
            id: customUAToolbar
            width: parent.width
            height: presetButton.height + 20
            anchors.bottom: parent.bottom
            color: "gray"
            ToolBarLayout
            {
                anchors.fill: parent

                ToolButton
                {
                    id: presetButton
                    text: "Well-known user agents"
                    width: 0.875 * parent.width
                    anchors.centerIn: parent
                    onClicked:
                    {
                        presetsLoader.source = "PresetsDialog.qml"
                        presetsLoader.item.open()
                    }
                }
            }
        }
    }
}
